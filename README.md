# Projet Sortir.com

### Description

Un site internet qui permet d'organiser des sorties et autres événements en groupes. 
Il a été réalisé dans le cadre d'un projet de groupe en formation de développeur Web et Web mobile à l'ENI Ecole informatique.
Le site est développé en PHP Symfony.


### Fonctionnalités

L'utilisation du site internet nécessite la création d'un compte utilisateur. Il est possible de modifier ses informations
personnelles, de demander un nouveau mot de passe en cas d'oubli ou de supprimer son compte.

Les utilisateurs peuvent créer un événement (ou une sortie), l'enregistrer, la publier. 
Une fois publiée, l'organisateur ne peut plus modifier la sortie mais peut l'annuler.

Ils peuvent consulter les sorties et filtrer les résultats suivant des mots-clés, une date, s'ils y participent, en sont l'organisateur ou encore si la sortie a déjà eu lieu.

Les utilisateurs peuvent s'inscrire à une sortie à venir (dans la limite des places disponibles fixée par l'organisateur) 
et s'en désister si elle n'a pas encore débuté.

Les administrateurs ont les mêmes droits que les utilisateurs, plus la possibilité de créer, modifier ou supprimer les villes, sites de ratachement ou utilisateurs.


### Captures

Accueil:

![liste sorties](projet_sortir_screenshots/project_sortir_liste_sortie.png)


Filtres:

![filtres](projet_sortir_screenshots/project_sortir_filtre.png)


Création sortie:

![création sortie](projet_sortir_screenshots/project_sortir_create.png)


Création sortie sur mobile:

![création sortie mobile](projet_sortir_screenshots/project_sortir_create-mobile.png)


Détail sortie orga:

![détail sortie](projet_sortir_screenshots/project_sortir_detail.png)


Détail sortie orga publiée:

![détail sortie publiée](projet_sortir_screenshots/project_sortir_detail_publiée.png)


Détail sortie participant:

![détail sortie participant](projet_sortir_screenshots/project_sortir_detail2.png)


Inscription sortie:

![inscription sortie](projet_sortir_screenshots/project_sortir_inscription.png)


Panneau admin utilisateurs:

![utilisateurs](projet_sortir_screenshots/project_sortir_user.png)


Panneau admin sites:

![sites](projet_sortir_screenshots/project_sortir_sites.png)


### Auteurs
 - Théo Le Visage
 - Arthur Ruby


### Statut

Le projet a été réalisé durant une période de deux semaines dans le cadre d'une formation.
Le but était de remplir un maximum des objectifs fixés. Il n'est pas prévu de le continuer.
