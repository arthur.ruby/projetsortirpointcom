<?php

namespace App\Controller;

use App\Entity\Etat;
use App\Entity\Event;
use App\Entity\User;
use App\Form\CancelType;
use App\Form\EventType;
use App\Form\SiteFilterType;
use App\Form\VilleType;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;


class EventController extends AbstractController
{
//    /**
//     * @Route("/", name="event_default")
//     * @param EntityManagerInterface $em
//     * @param Request $request
//     * @return Response
//     * @throws Exception
//     */
//    public function index(EntityManagerInterface $em, Request $request)
//    {
//        $formSite = $this->createForm(SiteFilterType::class);
//        $formSite->handleRequest($request);
//        dump($request);
//        if ($formSite->isSubmitted() && $formSite->isValid()) {
//            $eventId = (int)$request->request->get('site_filter')['site'];
//
//        }
//        else {
//            $eventId = null;
//        }
//
//        $eventRepo = $em->getRepository(Event::class);
//        $events = $eventRepo->findFirst30($eventId);
//        return $this->render('event/index.html.twig',
//            [
//                "title" => "Sorties",
//            	"events"=>$events,
//                'formSite' => $formSite->createView()
//            ]);
//    }

	/**
	 * @param EntityManagerInterface $em
	 * @param Request $request
	 * @param Security $security
	 * @return Response
	 * @throws Exception
	 * @Route(path="/", name="event_default")
	 */
    public function filter(EntityManagerInterface $em, Request $request, Security $security)
    {
	    $formSite = $this->createForm(SiteFilterType::class);
	    $formSite->handleRequest($request);
	    $userRepo = $em->getRepository(User::class);
	    $user = $userRepo->findOneBy(["pseudo"=>$security->getUser()->getUsername()]);


	    if ($formSite->isSubmitted() && $formSite->isValid())
	    {
		    $filters = $request->request->get('site_filter');
		    $eventRepo = $em->getRepository(Event::class);
		    $events = $eventRepo->findByFilters($filters, $user);
		    return $this->render('event/index.html.twig',
			    [
				    "title" => "Sorties",
				    "events"=>$events,
				    'formSite' => $formSite->createView()
			    ]);
	    }
	    else
	    {
		    $eventRepo = $em->getRepository(Event::class);
            $events = $eventRepo->findFirst30(null);
		    return $this->render('event/index.html.twig',
	            [
	                "title" => "Sorties",
	                "events"=>$events,
	                'formSite' => $formSite->createView()
	            ]);
	    }
    }

	/**
	 * @Route("/create", name="event_create")
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @param Security $security
	 * @return RedirectResponse|Response
	 */
    public function create(Request $request, EntityManagerInterface $em, Security $security)
    {
        $this->denyAccessUnlessGranted("ROLE_ACTIF", "", "Vous ne pouvez pas créer de nouvelle sortie: vous êtes désactivé");
    	$event = new Event();
        $eventForm = $this->createForm(EventType::class, $event);
        $villeForm = $this->createForm(VilleType::class);
        $eventForm->handleRequest($request);

        if ($eventForm->isSubmitted() && $eventForm->isValid()) {
            $userRepo = $em->getRepository(User::class);
            $username = $security->getUser()->getUsername();
            $user = $userRepo->findOneBy(["pseudo"=>$username]);

	        $etatEvent = $request->request->get('etatEvent');

            $event->setOrganisateur($user);
            $event->setSite($user->getSite());
            $user->addEventsOrganised($event);

            $etatRepo = $em->getRepository(Etat::class);
            $etat = $etatRepo->findOneBy(['libelle' => $etatEvent]);

            $event->setEtat($etat);

            $em->persist($event);
            $em->flush();
            $this->addFlash("success", "Vous avez bien réussi à ajouter un événement");
            $siteId = null;
            return $this->redirectToRoute("event_default");
        }
        return $this->render('event/create.html.twig', [
	        "title" => "Nouvelle Sortie",
	        'eventForm' => $eventForm->createview(),
            'villeForm' => $villeForm->createView(),
	        'event' => $event
            ]);
    }

	/**
	 * Détail de l'événement
	 * @Route("/{id}", name="event_detail", requirements={"id"="\d+"}, methods={"GET","POST"})
	 * @param $id l'identifiant de l'event à afficher
	 * @param Security $security
	 * @param EntityManagerInterface $em
	 * @return Response
	 * @throws Exception
	 */
    public function detail($id, Security $security, EntityManagerInterface $em)
    {
        // récupérer le detail de l'article dans la base
        $eventRepo = $em->getRepository(Event::class);
	    $user= $em->getRepository(User::class)
		          ->findOneBy(['pseudo'=>$security->getUser()->getUsername()]);
        $event = $eventRepo->find($id);
        if ($event == null)
        {
            throw $this->createNotFoundException("Sortie inconnue");
        }
	    $this->updateEtat($event, $em);
        $now = new DateTime();
        $mois = $now->sub(new DateInterval('P30D'));
        if($event->getDateHeureDebut()<$mois)
        {
            $this->addFlash("warning", "Cette sortie est terminée depuis plus d'un mois.");
        	return $this->redirectToRoute("event_default");
        }
	    if ($user->getId() != $event->getOrganisateur()->getId())
	    {

		    return $this->render("event/detail.html.twig", [
			    'event' => $event,
			    "title" => "Détail Sortie",
			    'isUserSession' => false
		    ]);
	    }
        return $this->render("event/detail.html.twig", [
            'event' => $event,
	        "title" => "Détail Sortie",
	        'isUserSession' => true
        ]);
    }

	/**
	 * Modifier un événement
	 * @Route("/{id}/edit", name="event_edit", requirements={"id"="\d+"})
	 * @param $id
	 * @param EntityManagerInterface $em
	 * @param Request $request
	 * @param Security $security
	 * @return RedirectResponse|Response
	 */
    public function edit($id, EntityManagerInterface $em, Request $request, Security $security)
    {
    	// récupérer l'article dans la base
	    $this->denyAccessUnlessGranted("ROLE_ACTIF", "", "Vous ne pouvez pas créer de nouvelle sortie: vous êtes désactivé");
	    $etatRepo = $em->getRepository(Etat::class);
	    $etatOuvert = $etatRepo->findOneBy(['libelle' => 'Ouverte']);
	    $etatCree = $etatRepo->findOneBy(['libelle' => 'Créée']);
	    $user= $em->getRepository(User::class)->findOneBy(['pseudo'=>$security->getUser()->getUsername()]);
	    $event = $em->getRepository(Event::class)->find($id);

	    if ($event==null)
        {
            throw $this->createNotFoundException("Evénement inconnu");
        }
	    if (!$this->isOrga($event,$user))
	    {
		    $this->addFlash("danger", "Vous ne pouvez pas modifier l'événement de quelqu'un!");
		    // redirection
		    return $this->redirectToRoute('event_detail',
			    [
				    'id' => $event->getId(),
				    "title" => "Détail Sortie"
			    ]);
	    }
        // instancier le formulaire
        $eventForm = $this->createForm(EventType::class, $event);
        $eventForm->handleRequest($request);
        if ($eventForm->isSubmitted() && $eventForm->isValid())
        {
	        $etatEvent = $request->request->get('etatEvent');

	        if ($this->checkEtat($event, $etatCree))
	        {
		        $etat = $etatRepo->findOneBy(['libelle' => $etatEvent]);
		        $event->setEtat($etat);
	        }
        	elseif ($this->checkEtat($event, $etatOuvert))
	        {
	        	if ("Ouverte" === $etatEvent)
		        {
		        	$event->setEtat($etatOuvert);
		        }
	        	else
		        {
		        	$event->setEtat($etatOuvert);
			        $this->addFlash("danger", "L'événement est déjà ouvert, il ne peut plus revenir à l'état \"Créé\".");
		        }
	        }
        	else
	        {
		        $this->addFlash("danger", "L'événement a déjà débuté, vous ne pouvez plus le modifier.");
		        return $this->redirectToRoute("event_detail", [
			        'id' => $event->getId(),
			        "title" => "Détail Sortie"
		        ]);
	        }

            $em->persist($event);
            $em->flush();

            $this->addFlash("success", "L'événement a bien été modifié");

            return $this->redirectToRoute("event_detail", [
                'id' => $event->getId(),
	            'event' => $event,
	            "title" => "Détail Sortie"
            ]);
        }
        return $this->render("event/edit.html.twig", [
	        "title" => "Modifier Sortie",
	        'event' => $event,
	        'eventForm'=> $eventForm->createView()
        ]);
    }

	/** Méthode en charge de publier une sortie (changer son statut de "créée" à "ouverte")
	 * @param $id
	 * @param EntityManagerInterface $em
	 * @param Security $security
	 * @return RedirectResponse
	 * @Route("/{id}/publish", name="event_publish", requirements={"id"="\d+"})
	 */
    public function publish($id, EntityManagerInterface $em, Security $security)
    {
	    $this->denyAccessUnlessGranted("ROLE_ACTIF", "", "Vous ne pouvez pas créer de nouvelle sortie: vous êtes désactivé");
	    $etatRepo = $em->getRepository(Etat::class);
    	$etatCree = $etatRepo->findOneBy(['libelle' => 'Créée']);
	    $user= $em->getRepository(User::class)->findOneBy(['pseudo'=>$security->getUser()->getUsername()]);
    	$event = $em->getRepository(Event::class)->find($id);
	    if ($event==null)
	    {
		    throw $this->createNotFoundException("Evénement inconnu");
	    }
	    if (!$this->isOrga($event,$user))
	    {
		    $this->addFlash("danger", "Vous ne pouvez pas publier l'événement de quelqu'un d'autre!");
		    // redirection
		    return $this->redirectToRoute('event_detail',
			    [
				    'id' => $event->getId(),
				    "title" => "Détail Sortie"
			    ]);
	    }
	    if (!$this->checkEtat($event, $etatCree))
	    {
		    $this->addFlash("danger", "Seul un événement en état créé peut être publié");
		    return $this->redirectToRoute("event_detail", [
			    'id' => $event->getId(),
			    "title" => "Détail Sortie"
		    ]);
	    }
	    $etatOuvert = $etatRepo->findOneBy(['libelle' => "Ouverte"]);
	    $event->setEtat($etatOuvert);

	    $em->persist($event);
	    $em->flush();

	    $this->addFlash("success", "L'événement a bien été publié");
	    return $this->redirectToRoute("event_detail", [
		    'id' => $event->getId(),
		    "title" => "Détail Sortie"
	    ]);
    }

	/**
	 * Supprimer un événement
	 * @Route("/{id}/cancel", name="event_cancel", requirements={"id"="\d+"})
	 * @param Request $request
	 * @param $id l'identifiant de l'event
	 * @param Security $security
	 * @param EntityManagerInterface $em
	 * @return Response
	 */
    public function cancel(Request $request, $id, Security $security, EntityManagerInterface $em)
        {
	        $this->denyAccessUnlessGranted("ROLE_ACTIF", "", "Vous ne pouvez pas créer de nouvelle sortie: vous êtes désactivé");
	        $event = $em->getRepository(Event::class)->find($id);
	        $etatOuvert = $em->getRepository(Etat::class)->findOneBy(['libelle' => 'Ouverte']);
	        $user= $em->getRepository(User::class)->findOneBy(['pseudo'=>$security->getUser()->getUsername()]);
	        if ($event==null) {
                throw $this->createNotFoundException("Evénement inconnu");
            }
	        if (!$this->isOrga($event,$user) && !$this->isGranted('ROLE_ADMIN'))
	        {
		        $this->addFlash("danger", "Vous ne pouvez pas annuler l'événement de quelqu'un d'autre espèce de nazi!");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			        [
				        'id' => $event->getId(),
				        "title" => "Détail Sortie"
			        ]);
	        }
            // instancier le formulaire
            $eventForm = $this->createForm(CancelType::class, $event);
            $eventForm->handleRequest($request);
            if ($eventForm->isSubmitted() && $eventForm->isValid())
            {
                if (!$this->checkEtat($event, $etatOuvert))
                {
                    $this->addFlash("danger", "L'événement a déjà débuté, vous ne pouvez plus l'annuler.");
                    return $this->redirectToRoute("event_detail", [
                        'id' => $event->getId(),
                        "title" => "Détail Sortie"
                    ]);
                }

                // sauvegarder les données dans la base
                $etatRepo = $em->getRepository(Etat::class);
                $etat = $etatRepo->findOneBy(["libelle"=>'Annulée']);
                $event->setEtat($etat);
                $em->persist($event);
                $em->flush();
                // ajout d'un message pour l'utilisateur
                $this->addFlash("success", "L'événement a bien été annulé.");
                // redirection
                return $this->redirectToRoute("event_default");
            }
            return $this->render("event/cancel.html.twig", [
                "title" => "Annuler sortie",
                'event' => $event,
                'eventForm'=> $eventForm->createView()
            ]);
        }

	/** Méthode en charge de vérifier si l'utilisateur session est l'orga de l'événement
	 * @param Event $event l'événement
	 * @param User $user l'utilisateur session
	 * @return bool true si l'utilisateur session est l'orga de l'événement
	 */
    private function isOrga(Event $event, User $user) : bool
    {
        $isOrga = false;
        if ($event->getOrganisateur() === $user)
        {
	        $isOrga = true;
        }
        return $isOrga;
    }

	/** Méthode en charge de vérifier si des places sont toujours disponibles pour les participants
	 * @param Event $event l'événement
	 * @return bool true si la capacité max n'est pas encore atteinte
	 */
    private function isAvailable(Event $event) : bool
    {
        $isAvailable = false;
        $capacite = $event->getNbInscriptionMax();
        $participants = sizeof($event->getUsers());
        if($participants < $capacite)
        {
            $isAvailable = true;
        }
        return $isAvailable;
    }

	/** Méthode en charge de vérifier si l'événement est ouvert aux inscriptions
	 * @param Event $event l'événement
	 * @param Etat $etat l'état de l'événement à vérifier
	 * @return bool true si l'état de l'événement égale celui passé en paramètre
	 */
    private function checkEtat(Event $event, Etat $etat)
    {
        $isOk = false;
        if($event->getEtat() === $etat)
        {
            $isOk = true;
        }
        return $isOk;
    }

	/** Méthode en cahrge de vérifier si un utilisateur participe déjà à un event
	 * @param Event $event l'event à vérifier
	 * @param User $user    l'utilisateur à vérifier
	 * @return bool true si l'utilisateur en param est déjà inscrit à l'evt en param
	 */
    private function checkDejaInscrit(Event $event, User $user)
    {
    	$dejaInscrit = false;
    	if($event->getUsers()->contains($user))
	    {
	    	$dejaInscrit = true;
	    }
    	return $dejaInscrit;
    }

	/** Méthode en charge de mettre à jour l'état d'un event en fonction de la date du jour
	 * @param Event $event
	 * @param EntityManagerInterface $em
	 * @throws Exception
	 */
    private function updateEtat(Event $event, EntityManagerInterface $em)
    {
	    if($event->getEtat()->getLibelle() !=="Annulée")
	    {
		    $etatRepo = $em->getRepository(Etat::class);
		    $dateNow = new DateTime('now');
		    $debutEvent = $event->getDateHeureDebut();
		    $finEvent = clone $debutEvent;
		    $finEvent->add($event->getDuree());
		    $finInscription = $event->getDateLimiteInscription();

		    if ($dateNow > $finInscription && $dateNow < $debutEvent)
		    {
			    $etat = $etatRepo->findOneBy(["libelle" => 'Clôturée']);
			    $event->setEtat($etat);
		    }
		    elseif ($dateNow > $debutEvent && $dateNow < $finEvent)
		    {
			    $etat = $etatRepo->findOneBy(["libelle" => 'Activité en cours']);
			    $event->setEtat($etat);
		    }
		    elseif ($dateNow > $finEvent)
		    {
			    $etat = $etatRepo->findOneBy(["libelle" => 'Passée']);
			    $event->setEtat($etat);
		    }
		    $em->persist($event);
		    $em->flush();
		}
    }

	/** Méthode permettant à un participant de s'inscrire à un event
	 * @param $id l'identifiant de l'event
	 * @param Security $security
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse
	 * @Route(path="/{id}/join", name="event_join", requirements={"id"="\d+"})
	 */
        public function addParticipant($id, Security $security, EntityManagerInterface $em)
        {
	        $this->denyAccessUnlessGranted("ROLE_ACTIF", "", "Vous ne pouvez pas vous inscrire à cette sortie: vous êtes désactivé");
	        $event = $em->getRepository(Event::class)->find($id);
	        $user= $em->getRepository(User::class)->findOneBy(['pseudo'=>$security->getUser()->getUsername()]);
			$etatOuvert = $em->getRepository(Etat::class)->findOneBy(['libelle' => 'Ouverte']);
	        if ($event == null)
	        {
		        throw $this->createNotFoundException("Sortie inconnue");
	        }

	        if ($this->isOrga($event,$user))
	        {
		        $this->addFlash("danger", "Vous ne pouvez pas vous inscrire à votre propre événement.");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			    [
			        'id' => $event->getId(),
			        "title" => "Détail Sortie"
		        ]);
	        }
	        if (!$this->checkEtat($event, $etatOuvert))
	        {
		        $this->addFlash("danger", "Vous ne pouvez pas vous inscrire. L'événement n'est pas ouvert.");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			        [
				        'id' => $event->getId(),
				        "title" => "Détail Sortie"
			        ]);
	        }
	        if (!$this->isAvailable($event))
	        {
		        $this->addFlash("danger", "Vous ne pouvez pas vous inscrire. L'événement est complet.");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			        [
				        'id' => $event->getId(),
				        "title" => "Détail Sortie"
			        ]);
	        }
	        if ($this->checkDejaInscrit($event, $user))
	        {
		        $this->addFlash("warning", "Vous êtes déjà inscrit à cet événement.");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			        [
				        'id' => $event->getId(),
				        "title" => "Détail Sortie"
			        ]);
	        }

	        $event->addUser($user);
	        $user->addEventInscrit($event);
	        $em->persist($event);
	        $em->persist($user);
	        $em->flush();
	        $this->addFlash("success", "L'inscription a bien été prise en compte");
	        // redirection
	        return $this->redirectToRoute('event_detail', [
		        'id' => $event->getId(),
		        "title" => "Détail Sortie"
	        ]);

        }

	/**
	 * @param $id
	 * @param Security $security
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse
	 * @Route(path="/{id}/withdraw", name="event_withdraw", requirements={"id"="\d+"})
	 */
        public function removeParticipant($id, Security $security, EntityManagerInterface $em)
        {
	        $this->denyAccessUnlessGranted("ROLE_ACTIF", "", "Vous ne pouvez pas vous désister: vous êtes désactivé");
	        $event = $em->getRepository(Event::class)->find($id);
	        $user= $em->getRepository(User::class)->findOneBy(['pseudo'=>$security->getUser()->getUsername()]);
	        $etatOuvert = $em->getRepository(Etat::class)->findOneBy(['libelle' => 'Ouverte']);

	        if ($event == null)
	        {
		        throw $this->createNotFoundException("Sortie inconnue");
	        }

	        if (!$this->checkEtat($event, $etatOuvert))
	        {
		        $this->addFlash("danger", "L'événement a commencé, vous ne pouvez plus vous désister.");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			        [
				        'id' => $event->getId(),
				        "title" => "Détail Sortie"
			        ]);
	        }

	        if (!$this->checkDejaInscrit($event, $user))
	        {
		        $this->addFlash("warning", "Vous ne pouvez pas vous désister de cet événement. Vous n'y êtes pas inscrit!");
		        // redirection
		        return $this->redirectToRoute('event_detail',
			        [
				        'id' => $event->getId(),
				        "title" => "Détail Sortie"
			        ]);
	        }
	        $event->removeUser($user);
	        $user->removeEventInscrit($event);
	        $em->persist($event);
	        $em->persist($user);
	        $em->flush();
	        $this->addFlash("success", "Le désistement a bien été pris en compte");
	        // redirection
	        return $this->redirectToRoute('event_detail', [
		        'id' => $event->getId(),
		        "title" => "Détail Sortie"
	        ]);
        }
}

