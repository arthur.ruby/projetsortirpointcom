<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AskForPasswordType;
use App\Form\ModifPasswordType;
use App\Form\ModifUserType;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
// remplacée par afficherDetailUtilisateur()
//	/**
//	 * @Route("/", name="profile")
//	 * @param EntityManagerInterface $em
//	 * @param Security $security
//	 * @return Response
//	 */
//    public function afficherProfil(EntityManagerInterface $em, Security $security)
//    {
//	    $userRepo = $em->getRepository(User::class);
//        $username = $security->getUser()->getUsername();
//	    $user = $userRepo->findOneBy(["pseudo"=>$username]);
//
//    	return $this->render("user/profile.html.twig",
//		    [
//                'title' => 'Profil',
//			    'utilisateur'=> $user
//            ]);
//    }

	/**
	 * Méthode en charge de vérifier si le pseudo passé en param correspond à celui de l'utilisateur en session
	 * @param Security $security
	 * @param $pseudo
	 * @return bool
	 */
    public function verifUtilSession(Security $security, $pseudo)
    {
	    $isUtilSession = false;

	    // vérification si l'utilisateur à afficher est le même que l'utilisateur connecté
	    $loginSession = $security->getUser()->getUsername();
	    if ($loginSession === $pseudo)
	    {
		    $isUtilSession = true;
	    }
	    return $isUtilSession;
    }

    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @param Security $security
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, Security $security) {

        $roles = $security->getUser()->getRoles();
        if ($roles[0] === 'ROLE_USER')
        {
	        throw $this->createAccessDeniedException("Vous n'avez pas les droits pour accéder à cette page. Piss off!");
        }
	    $user = new User();
        $userForm = $this->createForm(UserType::class, $user);

        // récupération de la requête
        // + injection des données du formulaire dans l'objet Article
        $userForm->handleRequest($request);
        // on vérifie si le formulaire a été soumis
        if ($userForm->isSubmitted() && $userForm->isValid()) {
	        $password = $encoder->encodePassword($user, $user->getMotDePasse());
	        $user->setMotDePasse($password);
        	$user->setAdmin(false);
            $user->setActif(true);
            $user->setSite(null);
            // sauvegarder les données dans la base
            $em->persist($user);
            $em->flush();
            // ajout d'un message pour l'utilisateur

            // redirection
            foreach ($roles as $role){
                if($role==='ROLE_ADMIN'){
                    $this->addFlash("success", "L'utilisateur a bien été ajouté");
                    return $this->redirectToRoute('user_gestion');
                }else{
                    $this->addFlash("success", "Vous avez bien réussi à créer votre compte! Vous pouvez maintenant vous connecter");
                    return $this->redirectToRoute("app_login");
                }
            }
        }
        return $this->render("user/user.html.twig",
	        [
		        'title' => "Inscription",
		        "nomBouton" => "S'inscrire!",
	        	'userForm' => $userForm->createView()
            ]);

    }

    /**
     * @Route(path="/gestion", name="gestion")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function gestion(EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', '', 'Accès refusé: seul un administrateur est autorisé à voir cette page');
    	$userRepo = $em->getRepository(User::class);
        $users = $userRepo->findAll();

        return $this->render('user/gestion.html.twig', [
            'users' => $users,
            'title' => 'Gestion des utilisateurs'
        ]);
    }

    /**
     * @Route(path="/{pseudo}/desactivate", name="desactivate")
     * @param $pseudo
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function deactivate(EntityManagerInterface $em, $pseudo){
	    $this->denyAccessUnlessGranted('ROLE_ADMIN', '', 'Accès refusé: seul un administrateur est autorisé à voir cette page');
	    $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBy(["pseudo"=>$pseudo]);
        if($user->getActif()){
            $user->setActif(false);
            $em->persist($user);
        }elseif (!$user->getActif()){
            $user->setActif(true);
            $em->persist($user);
        }
        $em->flush();
        return $this->redirectToRoute('user_detail', [
            'pseudo' => $pseudo
        ]);
    }

	/**
	 * @Route(path="/{pseudo}", name="detail")
	 * @param EntityManagerInterface $em
	 * @param Security $security
	 * @param $pseudo
	 * @return Response
	 */
    public function afficherDetailUtilisateur(EntityManagerInterface $em, Security $security, $pseudo)
    {
	    $userRepo = $em->getRepository(User::class);
	    $user = $userRepo->findOneBy(["pseudo"=>$pseudo]);
	    if ($user == null)
	    {
	    	throw $this->createNotFoundException("Utilisateur inconnu: cette page n'existe pas.");
	    }
	    else
	    {
		    $isUtilSession = $this->verifUtilSession($security, $pseudo);
            $isActivated = $user->getActif();
		    return $this->render("user/profile.html.twig",
			    [
				    'title' => 'Profil',
				    'utilisateur'=> $user,
				    'isUtilSession' => $isUtilSession,
                    'isActivated' => $isActivated
			    ]);
	    }
    }


    /**
     * @param $pseudo
     * @param EntityManagerInterface $em
     * @param Request $requete
     * @param Security $security
     * @return Response
     * @Route(path="/{pseudo}/edit", name="modif")
     */
    public function update($pseudo, EntityManagerInterface $em, Request $requete, Security $security)
    {
//	    dump($requete);
	    $this->denyAccessUnlessGranted("IS_AUTHENTICATED_FULLY", '', 'Accès refusé: vous devez être connecté pour voir cette page');
	    $userRepo = $em->getRepository(User::class);
	    $user = $userRepo->findOneBy(["pseudo"=>$pseudo]);
	    if ($user == null)
	    {
		    throw $this->createNotFoundException("Utilisateur inconnu: cette page n'existe pas.");
	    }
	    $isUtilSession = $this->verifUtilSession($security, $pseudo);
	    if ($isUtilSession)
	    {
		    $modifUserForm = $this->createForm(ModifUserType::class, $user);
		    $modifUserForm->handleRequest($requete);
		    if ($modifUserForm->isSubmitted() && $modifUserForm->isValid())
		    {
//			    $plainPassword = $requete->request->get('confirmPass');
//		    	$passForm = $encoder->encodePassword($user, $plainPassword);
//		    	$passBDD = $encoder->encodePassword($user, $user->getMotDePasse());
//		    	if($passBDD !== $passForm)
//			    {
//				    $this->addFlash("danger", "Erreur! Mot de passe invalide!!");
//				    return $this->render("user/edit.html.twig",
//					    [
//						    'title' => "Modification profil",
//						    "nomBouton" => "Modifier!",
//						    "utilisateur" => $user,
//						    "modifUserForm" => $modifUserForm->createView()
//					    ]);
//			    }
//		    	else
//			    {
			        $em->persist($user);
			        $em->flush();
			        $this->addFlash("success","Wololo! Modification du profil réussie!!");
				    return $this->render("user/profile.html.twig",
					    [
						    'title' => "Détail profil",
						    "utilisateur" => $user,
						    'isUtilSession' => $isUtilSession
				        ]
				    );
//			    }

		    }
		    return $this->render("user/edit.html.twig",
			    [
			        'title' => "Modification profil",
				    "nomBouton" => "Modifier!",
				    "modifUserForm" => $modifUserForm->createView()
			    ]);
	    }
	    else
	    {
		    throw $this->createAccessDeniedException("Impossible d'accéder à la modification de cet utilisateur.");
	    }
    }

	/**
	 * @param $pseudo
	 * @param EntityManagerInterface $em
	 * @param Request $requete
	 * @param Security $security
	 * @param UserPasswordEncoderInterface $encoder
	 * @return Response
	 * @Route(path="/{pseudo}/edit/password", name="modif_pass")
	 */
    public function updatePassword($pseudo, EntityManagerInterface $em, Request $requete, Security $security, UserPasswordEncoderInterface $encoder)
    {
	    $this->denyAccessUnlessGranted("IS_AUTHENTICATED_FULLY", '', 'Accès refusé: vous devez être connecté pour voir cette page');
	    $userRepo = $em->getRepository(User::class);
	    $user = $userRepo->findOneBy(["pseudo"=>$pseudo]);
	    if ($user == null)
	    {
		    throw $this->createNotFoundException("Utilisateur inconnu: cette page n'existe pas.");
	    }
	    $isUtilSession = $this->verifUtilSession($security, $pseudo);
	    if ($isUtilSession)
	    {
		    $modifPassForm = $this->createForm(ModifPasswordType::class, $user);
		    $modifPassForm->handleRequest($requete);
		    if ($modifPassForm->isSubmitted() && $modifPassForm->isValid())
		    {
			    $password = $encoder->encodePassword($user, $user->getMotDePasse());
			    $user->setMotDePasse($password);
		    	$em->persist($user);
			    $em->flush();
			    $this->addFlash("success","Wololo! Modification du mot de passe réussie!!");
			    return $this->render("user/profile.html.twig",
				    [
					    'title' => "Détail profil",
					    "utilisateur" => $user,
					    'isUtilSession' => $isUtilSession
				    ]
			    );
	        }

	    return $this->render("user/editPass.html.twig",
		    [
			    'title' => "Modification profil",
			    "nomBouton" => "Modifier!",
			    "modifPassForm" => $modifPassForm->createView()
		    ]);
        }
		else
		{
			throw $this->createAccessDeniedException("Impossible d'accéder à la modification de cet utilisateur.");
		}
    }

	/**
	 * @param $pseudo
	 * @param EntityManagerInterface $em
	 * @param Security $security
	 * @return Response
	 * @Route(path="/{pseudo}/delete", name="delete")
	 */
    public function delete($pseudo, EntityManagerInterface $em, Security $security)
    {
	    $userRepo = $em->getRepository(User::class);
	    $user = $userRepo->findOneBy(["pseudo"=>$pseudo]);
	    if ($user == null)
	    {
		    throw $this->createNotFoundException("Utilisateur inconnu: cette page n'existe pas.");
	    }
	    $isUtilSession = $this->verifUtilSession($security, $pseudo);
	    $rolesSession = $security->getUser()->getRoles();
        foreach ($rolesSession as $role){
            if($role==='ROLE_ADMIN'){
                $em->remove($user);
                $em->flush();
                $this->addFlash("danger", "Le profil a bien été supprimé");
                return $this->redirectToRoute('user_gestion');
            }
        }
	    if ($isUtilSession)
	    {
		    $em->remove($user);
		    $em->flush();
		    $this->addFlash("danger", "Zololo! Suppression du profil réussie!!");
		    return $this->redirectToRoute('app_logout');
	    }
	    else
	    {
		    throw $this->createAccessDeniedException("Impossible d'accéder à la suppression de cet utilisateur.");
	    }

    }

}
