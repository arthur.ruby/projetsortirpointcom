<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AskForPasswordType;
use App\Form\ModifPasswordType;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
	/**
	 * @Route("/login", name="app_login")
	 * @param AuthenticationUtils $authenticationUtils
	 * @return Response
	 */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

	/**
	 * méthode en charge de demander un lien de réinitialisation du mot de passe
	 * @param EntityManagerInterface $em
	 * @param Request $requete
	 * @param Security $security
	 * @return RedirectResponse|Response
	 * @Route(path="/password/ask", name="ask_password")
	 */
	public function askForNewPassword(EntityManagerInterface $em, Request $requete, Security $security)
	{
		// On vérifie que l'utilisateur n'est pas déjà connecté
		if($security->getUser())
		{
			$roles = $security->getUser()->getRoles();
			foreach ($roles as $role)
			{
				if ($role === 'ROLE_USER' || $role === 'ROLE_ADMIN')
				{
					throw $this->createAccessDeniedException("Vous n'avez pas les droits pour accéder à cette page. Piss off!");
				}
			}
		}

		// Création d'un formulaire qui récupère un email
		$askPassForm = $this->createForm(AskForPasswordType::class);
		$askPassForm->handleRequest($requete);

		if ($askPassForm->isSubmitted() && $askPassForm->isValid())
		{
			// On vérifie que l'email récupéré correspond bien à un utilisateur enregistré
			$params = $requete->request->get('ask_for_password');
			$email = $params['email'];
			$userRepo = $em->getRepository(User::class);
			$user = $userRepo->findOneBy(["email"=>$email]);
			if ($user == null)
			{
				$this->addFlash("danger","Erreur: Cet email est introuvable dans notre base de données");
				return $this->redirectToRoute("ask_password");
			}

			// On concatène le pseudo, l'id et l'email de l'utilisateur dans une string "code"
			$code = $user->getPseudo() . $user->getId() . $user->getEmail();

			// Puis on hash cette string en md5
			$hash = hash('md5', $code);

			// Puis on concatenne le hash et l'id de l'utilisateur
			// Et on inverse le sens de la string
			$hashcode = strrev($hash . (string)$user->getId());

			// Enfin, on l'envoie vers la vue qui symbolise un email reçu par l'utilisateur
			$this->addFlash("success","Si cette adresse est enregistrée dans notre base de données, vous allez recevoir un lien de réinitialisation.");
			return $this->render("security/resetPassLink.html.twig",
				[
					'hashcode' => $hashcode,
					'email' => $email
				]);
		}

		return $this->render("security/askNewPass.html.twig",
			[
				'title' => "Ask for Password",
				"nomBouton" => "Demander",
				"askPassForm" => $askPassForm->createView()
			]);
	}

	/**
	 * Méthode en charge de réinitialiser le mot de passe quand il a été oublié
	 * @param string $hashcode
	 * @param EntityManagerInterface $em
	 * @param Request $requete
	 * @param UserPasswordEncoderInterface $encoder
	 * @return RedirectResponse|Response
	 * @Route(path="/password/reset/{hashcode}", name="reset_password")
	 */
	public function resetPassword(string $hashcode, EntityManagerInterface $em, Request $requete, UserPasswordEncoderInterface $encoder)
	{
		// On récupère le code du lien de réinitialisation transmis par email
		// On vérifie qu'il fait la bonne taille
		if (strlen($hashcode) <= 32)
		{
			throw $this->createNotFoundException("Erreur: cette page n'existe pas.");
		}
		// On le remet dans le bon sens et on sépare le hash de l'id_utilisateur
		$tableauHash = str_split(strrev($hashcode), 32);
		$hash = $tableauHash[0];
		$id = $tableauHash[1];
		// On cherche l'utilisateur correspondant à l'id
		$userRepo = $em->getRepository(User::class);
		$user = $userRepo->find($id);
		// On crée un hash avec les mêmes informations que dans la méthode askForNewPassword
		$code = $user->getPseudo() . $user->getId() . $user->getEmail();
		$verifHash = hash('md5', $code);

		// Puis on vérifie que ce nouveau hash est identique à celui transmis en paramètre
		if ($hash !== $verifHash)
		{
			$this->addFlash("danger","Erreur: Cet email est introuvable dans notre base de données");
			return $this->redirectToRoute("ask_password");
		}
		//Si oui, on crée le formulaire de modification de mot de passe
		$resetPassForm = $this->createForm(ModifPasswordType::class, $user);
		$resetPassForm->handleRequest($requete);
		// S'il est valide et soumis, on modifie le mdp
		if ($resetPassForm->isSubmitted() && $resetPassForm->isValid())
		{
			$password = $encoder->encodePassword($user, $user->getMotDePasse());
			$user->setMotDePasse($password);
			$em->persist($user);
			$em->flush();
			$this->addFlash("success","Wololo! Modification du mot de passe réussie!! Vous pouvez à présent vous connecter");
			return $this->redirectToRoute("app_login");
		}

		return $this->render("security/resetPass.html.twig",
			[
				'title' => "Réinitialisation Mot de passe",
				"nomBouton" => "Envoyer",
				"resetPassForm" => $resetPassForm->createView()
			]);
	}
}
