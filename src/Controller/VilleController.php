<?php

namespace App\Controller;

use App\Entity\Ville;
use App\Form\NewVilleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VilleController
 * @package App\Controller
 * @Route(path="/ville", name="ville_")
 */
class VilleController extends AbstractController
{
	/**
	 * @Route("/", name="index")
	 * @param EntityManagerInterface $em
	 * @return Response
	 */
    public function index(EntityManagerInterface $em)
    {
        $villeRepo = $em->getRepository(Ville::class);
        $villes = $villeRepo->findAll();

    	return $this->render('ville/ville.html.twig',
	        [
                'title' => 'Villes',
		        'villes' => $villes
            ]);
    }

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return Response
	 * @Route(path="/new", name="create")
	 */
    public function create(Request $request, EntityManagerInterface $em)
    {
	    $this->denyAccessUnlessGranted("ROLE_ADMIN", 'Zone interdite', "Vous n'avez rien à faire icitte!");
    	$ville = new Ville();
	    $villeForm = $this->createForm(NewVilleType::class, $ville);
	    $villeForm->handleRequest($request);
	    if ($villeForm->isSubmitted() && $villeForm->isValid())
	    {
		    $em->persist($ville);
		    $em->flush();

		    $this->addFlash("success", "Vous avez bien réussi à ajouter une nouvelle ville");
		    // redirection
		    return $this->redirectToRoute("ville_index");
	    }
	    return $this->render("ville/create.html.twig",
		    [
	            'title' => 'Nouvelle Ville',
			    "nomBouton" => "Enregistrer",
			    'villeForm' => $villeForm->createView()
            ]);
    }

	/** Méthode en charge de supprimer une ville
	 * @param $id l'identifiant de la ville à supprimer
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse
	 * @Route(path="/delete/{id}", name="delete", requirements={"id"="\d+"})
	 */
    public function remove($id, EntityManagerInterface $em)
    {
	    $this->denyAccessUnlessGranted("ROLE_ADMIN", 'Zone interdite', "Vous n'avez rien à faire icitte!");

	    $ville = $em->getRepository(Ville::class)->find($id);
	    if ($ville == null)
	    {
		    throw $this->createNotFoundException("Ville inconnue: cette page n'existe pas.");
	    }
	    $em->remove($ville);
	    $em->flush();
	    $this->addFlash("success", "Wololo! La ville a bien été rayée de la carte!");
	    return $this->redirectToRoute("ville_index");
    }

	/**
	 * @param $id
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse|Response
	 * @Route(path="/update/{id}", name="update", requirements={"id"="\d+"})
	 */
    public function update($id, Request $request, EntityManagerInterface $em)
    {
	    $this->denyAccessUnlessGranted("ROLE_ADMIN", 'Zone interdite', "Vous n'avez rien à faire icitte!");

	    $ville = $em->getRepository(Ville::class)->find($id);
	    $modifVilleForm = $this->createForm(NewVilleType::class, $ville);
	    $modifVilleForm->handleRequest($request);

	    if ($ville == null)
	    {
		    throw $this->createNotFoundException("Ville inconnue: cette page n'existe pas.");
	    }

	    if ($modifVilleForm->isSubmitted() && $modifVilleForm->isValid())
	    {
		    $em->persist($ville);
		    $em->flush();
		    $this->addFlash("success", "Xololo! La ville a bien été modifiée!");
		    return $this->redirectToRoute("ville_index");
	    }
	    return $this->render("ville/edit.html.twig",
		    [
			    'title' => "Modification Ville",
			    "nomBouton" => "Modifier!",
			    "modifVilleForm" => $modifVilleForm->createView()
		    ]);
    }
}
