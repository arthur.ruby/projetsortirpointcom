<?php

namespace App\Controller;

use App\Entity\Site;
use App\Form\SiteType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SiteController
 * @package App\Controller
 * @Route(path="/site", name="site_")
 */
class SiteController extends AbstractController
{
	/**
	 * @Route("/", name="index")
	 * @param EntityManagerInterface $em
	 * @return Response
	 */
	public function index(EntityManagerInterface $em)
	{
		$sites = $em->getRepository(Site::class)->findAll();
		return $this->render('site/site.html.twig',
			[
				'title' => 'Sites',
				'sites' => $sites
			]);
	}

	/**
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse|Response
	 * @Route(path="/create", name="new")
	 */
	public function create(Request $request, EntityManagerInterface $em)
	{
		$this->denyAccessUnlessGranted("ROLE_ADMIN", 'Zone interdite', "Vous n'avez rien à faire icitte!");
		$site = new Site();
		$siteForm = $this->createForm(SiteType::class, $site);
		$siteForm->handleRequest($request);

		if ($siteForm->isSubmitted() && $siteForm->isValid())
		{
			$em->persist($site);
			$em->flush();

			$this->addFlash("success", "Vous avez bien réussi à ajouter un nouveau site");
			return $this->redirectToRoute("site_index");
		}
		return $this->render("site/create.html.twig",
			[
				'title' => 'Nouveau Site',
				"nomBouton" => "Créer",
				'siteForm' => $siteForm->createView()
			]);
	}

	/**
	 * @param $id
	 * @param Request $request
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse|Response
	 * @Route(path="/update/{id}", name="update", requirements={"id"="\d+"})
	 */
	public function update($id, Request $request, EntityManagerInterface $em)
	{
		$this->denyAccessUnlessGranted("ROLE_ADMIN", 'Zone interdite', "Vous n'avez rien à faire icitte!");
		$site = $em->getRepository(Site::class)->find($id);
		if ($site == null)
		{
			throw $this->createNotFoundException("Site inconnu: cette page n'existe pas.");
		}

		$modifSiteForm = $this->createForm(SiteType::class, $site);
		$modifSiteForm->handleRequest($request);

		if ($modifSiteForm->isSubmitted() && $modifSiteForm->isValid())
		{
			$em->persist($site);
			$em->flush();

			$this->addFlash("success", "Vous avez bien réussi à modifier le site");
			return $this->redirectToRoute("site_index");
		}
		return $this->render("site/create.html.twig",
			[
				'title' => 'Modifier Site',
				"nomBouton" => "Modifier",
				'siteForm' => $modifSiteForm->createView()
			]);
	}

	/**
	 * @param $id
	 * @param EntityManagerInterface $em
	 * @return RedirectResponse
	 * @Route(path="/delete/{id}", name="delete", requirements={"id"="\d+"})
	 */
	public function remove($id, EntityManagerInterface $em)
	{
		$this->denyAccessUnlessGranted("ROLE_ADMIN", 'Zone interdite', "Vous n'avez rien à faire icitte!");
		$site = $em->getRepository(Site::class)->find($id);
		if ($site == null)
		{
			throw $this->createNotFoundException("Site inconnu: cette page n'existe pas.");
		}
		$em->remove($site);
		$em->flush();
		$this->addFlash("success", "Wololo! Le site a bien été supprimé!");
		return $this->redirectToRoute("site_index");
	}

}
