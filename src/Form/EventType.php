<?php

namespace App\Form;

use App\Entity\Event;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $dateNow = new DateTime('now');
	    $dateMin = ((int)$dateNow->format("Y"));
	    $dateMax = ((int)$dateNow->format("Y")) + 10;
    	$builder
            ->add('nom', TextType::class,
                [
                'attr' =>
			            [
                            'placeholder' => "Nom de l'événement"
                        ]
                ])
            ->add('lieu', EntityType::class, [
                'class' => "App\Entity\Lieu",
                'choice_label' => "nom",
                'placeholder' => "Sélectionner un lieu",
                'expanded' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->orderBy('d.nom', 'desc');
                }
            ])
            ->add('dateHeureDebut', DateTimeType::class,
                [
                    'label' => 'Date et heure de début',
	                'years' => range($dateMin,$dateMax),
                ])
            ->add('duree', DateIntervalType::class,
                [
                    'label' => 'Durée de l\'événement',
                    'labels' => [
                        'invert' => null,
                        'years' => null,
                        'months' => null,
                        'weeks' => null,
                        'days' => 'Nombre de jours',
                        'hours' => 'Nombre d\'heures',
                        'minutes' => 'Nombre de minutes',
                        'seconds' => null,
                    ],
                    'with_years'  => false,
                    'with_months' => false,
                    'with_days'   => true,
                    'with_hours'  => true,
                    'with_minutes'=> true,
                    'days' => range(0, 31)
                ])
            ->add('dateLimiteInscription', DateTimeType::class,
                [
                    'label' => 'Date limite d\'inscription',
	                'years' => range($dateMin,$dateMax),
                ])
            ->add('nbInscriptionMax', IntegerType::class,
                [
                    'attr' =>
                        [
                            'placeholder' => "Nombre d'inscrits maximum"
                        ]
                ])
            ->add('infoEvent', TextareaType::class,
                [
                    'attr' =>
                        [
                            'placeholder' => "Description"
                        ]
                ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
