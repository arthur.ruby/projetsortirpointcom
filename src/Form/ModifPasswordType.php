<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModifPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('motDePasse', RepeatedType::class,
		        [
			        'type' => PasswordType::class,
			        'invalid_message' => "Les deux mots de passe sont différents",
			        'first_options' =>
				        [
					        'label' => 'Nouveau Mot de Passe',
					        'attr' =>
						        [
							        'placeholder' => 'Mot de passe...'
						        ]
				        ],
			        'second_options' =>
				        [
					        'label' => 'Confirmation',
					        'attr' =>
						        [
							        'placeholder' => 'Confirmation...'
						        ]
				        ]
		        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
