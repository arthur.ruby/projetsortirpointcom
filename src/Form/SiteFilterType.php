<?php

namespace App\Form;

use App\Entity\Site;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dateNow = new DateTime('now');
        $dateMin = ((int)$dateNow->format("Y")) - 1;
        $dateMax = ((int)$dateNow->format("Y")) + 10;

    	$builder
            ->add('site', EntityType::class, [
                'class' => Site::class,
                'choice_label' => 'nom',
                'placeholder' => "--Selectionner un site--",
                'required'=> false]
            )
	        ->add('recherche', SearchType::class,
		        [
		        	'label' => 'Recherche:',
			        'required' => false,
			        'attr' =>
			        [
			        	'placeholder' => 'La sortie contient...'
			        ]
		        ])
	        ->add('dateMin', DateType::class,
		        [
		        	'label' => "entre le:",
			        'required' => false,
			        'years' => range($dateMin,$dateMax),
			        'placeholder' =>
				    [
				        'year' => 'Année',
					    'month' => 'Mois',
					    'day' => 'Jour'
			        ],
		        ])
	        ->add('dateMax', DateType::class,
		        [
			        'label' => "et le:",
			        'required' => false,
			        'years' => range($dateMin,$dateMax),
			        'placeholder' =>
				        [
					        'year' => 'Année',
					        'month' => 'Mois',
					        'day' => 'Jour'
				        ],

	            ])
	        ->add('orga', CheckboxType::class,
		        [
		            'label' => "Je suis l'organisateur",
			        'label_attr' =>
				        [
					        'class' => 'custom-control-label'
				        ],
			        'required' => false,
			        'attr' =>
				        [
					        'class' => 'custom-control-input'
				        ]
		        ])
	        ->add('inscrit', CheckboxType::class,
		        [
			        'label' => "Je suis inscrit(e)",
			        'label_attr' =>
				        [
					        'class' => 'custom-control-label'
				        ],
			        'required' => false,
			        'attr' =>
				        [
					        'class' => 'custom-control-input'
				        ]
		        ])
	        ->add('pasInscrit', CheckboxType::class,
		        [
			        'label' => "Je ne suis pas inscrit(e)",
			        'label_attr' =>
				        [
					        'class' => 'custom-control-label'
				        ],
			        'required' => false,
			        'attr' =>
				        [
					        'class' => 'custom-control-input'
				        ]
		        ])
	        ->add('passee', CheckboxType::class,
		        [
			        'label' => "Sorties passées",
			        'label_attr' =>
			        [
			        	'class' => 'custom-control-label'
			        ],
			        'required' => false,
			        'attr' =>
			        [
			        	'class' => 'custom-control-input'
			        ]
		        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
