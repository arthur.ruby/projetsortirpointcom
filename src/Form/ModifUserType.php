<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ModifUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('nom', TextType::class,
		        [
			        'label' => 'Nom',
			        'attr' =>
				        [
					        'placeholder' => "Votre nom..."
				        ]
		        ])
	        ->add('prenom', TextType::class,
		        [
			        'label' => 'Prénom',
			        'attr' =>
				        [
					        'placeholder' => "Votre prénom..."
				        ]
		        ])
	        ->add('pseudo', TextType::class,
		        [
			        'label' => 'Pseudo',
			        'attr' =>
				        [
					        'placeholder' => "Votre pseudo..."
				        ]
		        ])
	        ->add('telephone', TelType::class,
		        [
			        'label' => 'Téléphone',
			        'attr' =>
				        [
					        'pattern' => '[0-9]{10,13}',
					        'placeholder' => '0299...'
				        ]
		        ])
	        ->add('email', EmailType::class,
		        [
			        'label' => 'Email',
			        'attr' =>
				        [
					        'placeholder' => "exemple@exemple.com..."
				        ]
		        ])
            ->add('imageFile', VichImageType::class)
            ->add('site', EntityType::class, [
                'class' => "App\Entity\Site",
                'choice_label' => "nom",
                'label'=> 'Ville De Rattachement',
                'placeholder' => "Sélectionner une ville de rattachement",
                'expanded' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.nom', 'desc');
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
