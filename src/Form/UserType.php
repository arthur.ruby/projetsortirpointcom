<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class,
	            [
		            'label' => 'Nom',
		            'attr' =>
			            [
				            'placeholder' => "Votre nom..."
			            ]
	            ])
            ->add('prenom', TextType::class,
	            [
		            'label' => 'Prénom',
		            'attr' =>
			            [
				            'placeholder' => "Votre prénom..."
			            ]
	            ])
	        ->add('pseudo', TextType::class,
	            [
		            'label' => 'Pseudo',
		            'attr' =>
			            [
				            'placeholder' => "Votre pseudo..."
			            ]
	            ])
            ->add('telephone', TelType::class,
	            [
		            'label' => 'Téléphone',
		            'attr' =>
			            [
				            'pattern' => '[0-9]{10,13}',
				            'placeholder' => '0299...'
			            ]
	            ])
            ->add('email', EmailType::class,
	            [
		            'label' => 'Email',
		            'attr' =>
			            [
				            'placeholder' => "exemple@exemple.com..."
			            ]
	            ])
            ->add('motDePasse', RepeatedType::class,
	            [
		            'type' => PasswordType::class,
		            'invalid_message' => "Les deux mots de passe sont différents",
		            'first_options' =>
			            [
				            'label' => 'Mot de Passe',
				            'attr' =>
					            [
						            'placeholder' => 'Mot de passe...'
					            ]
			            ],
		            'second_options' =>
			            [
				            'label' => 'Confirmation',
				            'attr' =>
					            [
						            'placeholder' => 'Confirmation...'
					            ]
			            ]
	            ])
            ->add('imageFile', VichImageType::class, [
                'required'=>false
            ] )
            ->add('site', EntityType::class, [
                'class' => "App\Entity\Site",
                'choice_label' => "nom",
                'placeholder' => "Sélectionner une ville de rattachement",
                'expanded' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.nom', 'desc');
                }
            ])
//            ->add('site', TextType::class,
//	            [
//		            'label' => 'Site',
//		            'attr' =>
//			            [
//				            'placeholder' => "Votre Site..."
//			            ]
//	            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
