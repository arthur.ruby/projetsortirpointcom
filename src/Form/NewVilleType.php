<?php

namespace App\Form;

use App\Entity\Ville;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewVilleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,
	            [
					'label' => 'Ville',
		            'attr' =>
			            [
				            'placeholder' => "Nom de la ville..."
			            ]
	            ])
            ->add('codePostal', TextType::class,
	            [
	            	'label' => 'Code postal',
		            'attr' =>
			            [
				            'placeholder' => "Son code postal..."
			            ],
	            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ville::class,
        ]);
    }
}
