<?php

namespace App\Repository;

use App\Entity\Event;
use App\Entity\User;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Exception;
use http\Exception\RuntimeException;


/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @param $siteId
     * @return Event[] Returns an array of Event objects
     * @throws Exception
     */
    public function findFirst30($siteId)
    {
        $plusMois = new DateTime();
        $plusMois->sub(new DateInterval('P30D'));
        $qb = $this->createQueryBuilder('e')
            ->andWhere('e.dateHeureDebut > :limiteArchive')
            ->setParameter('limiteArchive', $plusMois)
            ->orderBy('e.dateHeureDebut', 'DESC');
            if ($siteId != null) {
                $qb->andWhere('e.site = :siteId')
                    ->setParameter('siteId', $siteId);
            }
            $qb->setMaxResults(30);
            $query = $qb->getQuery();
            return $query->getResult();
    }

	/** Méthode en charge d'appliquer les filtres de recherche sur la liste des sorties
	 * @param array $filters tableaux de paramètres récupérés du formulaire de filtres
	 * @param User $user l'utilisateur de la session pour comparer aux champs orga, inscrit
	 * @return mixed
	 * @throws Exception
	 */
    public function findByFilters(array $filters, User $user)
    {
        $resultat = null;
        $qb = $this->surFiltre($filters);
        $pasInscrit = $this->pasInscrit($filters, $user, $qb);
        $orga = $this->orga($filters, $user, $qb);
        $inscrit = $this->inscrit($filters, $user, $qb);
        $passe = $this->passee($filters, $qb);
        if($pasInscrit != null){
            $resultat = array_merge((array)$resultat, $pasInscrit);
        }
        if($orga != null){
            $resultat = array_merge((array)$resultat, $orga);
        }
        if($inscrit != null){
            $resultat = array_merge((array)$resultat, $inscrit);
        }
        if($passe != null){
            $resultat = array_merge((array)$resultat, $passe);
        }
        if($resultat==null){
            $query = $qb->getQuery();
            $result = $query->getResult();
            $resultat = array_merge((array)$resultat, $result);
        }

        $resultat = array_unique($resultat);

	    return $resultat;
    }

    public function pasInscrit(array $filters, User $user, QueryBuilder $qb){
        if (isset($filters['pasInscrit'])) {
            // Cette sous-requête renvoie la liste des identifiants d'Event auxquels l'utilisateur
            // n'est pas inscrit et desquels il n'est pas l'orga
            $subq = $this->getEntityManager()->createQuery
            ('SELECT DISTINCT e.id
                        FROM App:Event e
                        LEFT JOIN e.users ue
                        WHERE ue.id = :uId 
                            OR e.organisateur = :uId
                    ')
                ->setParameter('uId', $user->getId());

            $res = $subq->getResult();
            // On initialise une chaîne qui va contenir le résultat
            $strRes = "";

            $nbRes = sizeof($res);
            if ($nbRes > 0) {
                $i = 0;
                foreach ($res as $couple) {
                    $i++;
                    $strRes .= $couple['id'] . ($i < $nbRes ? ', ' : "");
                }
                $qb->andWhere
                (
                    $qb->expr()->notIn('ev.id', $strRes)
                );
            }
            $query = $qb->getQuery();
            return $query->getResult();
        } else {
            return null;
        }
    }
    public function orga(array $filters, User $user, QueryBuilder $qb){
        // Si checkbox "evts dont je suis organisateur" cochée
        if (isset($filters['orga'])) {
            $qb->andWhere('ev.organisateur = :orga')
                ->setParameter('orga', $user->getId());

            $query = $qb->getQuery();
            return $query->getResult();
        } else {
            return null;
        }
    }

    public function inscrit(array $filters, User $user, QueryBuilder $qb){
        // Si checkbox "evts où je suis inscrit" cochée
        if (isset($filters['inscrit'])) {
            $qb->join('ev.users', 'us')
                ->andWhere('us.id = :userId')
                ->setParameter('userId', $user->getId());

            $query = $qb->getQuery();
            return $query->getResult();
        } else {
            return null;
        }
    }

    public function passee(array $filters, QueryBuilder $qb){
        // Si checkbox "evts passés" cochée
        // l'id de l'état "passée" est le n°5
        if (isset($filters['passee']))
        {
            $qb->join('ev.etat', 'et')
                ->andWhere('et.libelle = :etat')
                ->setParameter('etat','Passée');

            $query = $qb->getQuery();
            return $query->getResult();
        }else{
            return null;
        }
    }

    public function surFiltre(array $filters){
        $qb = $this->createQueryBuilder('ev');
        // Si sélection der site dans menu déroulant
        if($filters['site'])
        {
            $qb->andWhere('ev.site = :site')
                ->setParameter('site', $filters['site']);
        }
        // Si champs recherche renseigné
        if($filters['recherche'])
        {
            $qb->andWhere('ev.nom like :nom or ev.infoEvent like :nom')
                ->setParameter('nom', '%' . $filters['recherche'] . '%');
        }
        // Si date min renseignée:
        // Le formulaire renvoie un tableau de filtres ($filters) et le filtre [dateMin] est lui-même un tableau
        // On ne peut donc pas juste vérifier si 'dateMin!=null' car ce n'est jamais le cas: il possède trois
        // enregistrements: [year], [month] et [day] qui eux peuvent être vides.

        if($filters['dateMin']['year'] && $filters['dateMin']['month'] && $filters['dateMin']['day'])
        {
            $annee = $filters['dateMin']['year'];
            $mois = $filters['dateMin']['month'];
            $jour = $filters['dateMin']['day'];
            $strDateMin = $annee . '-' . $mois . '-' . $jour  . ' 00:00';
            try {
                $dateMin = new DateTime($strDateMin);
            } catch (Exception $e) {
                throw new RuntimeException('Los filtros no funcionaron bien');
            }

            $qb->andWhere('ev.dateHeureDebut > :dateMin')
                ->setParameter('dateMin', $dateMin);
        }
        // Si date max renseignée:
        if($filters['dateMax']['year'] && $filters['dateMax']['month'] && $filters['dateMax']['day'])
        {
            $annee = $filters['dateMax']['year'];
            $mois = $filters['dateMax']['month'];
            $jour = $filters['dateMax']['day'];
            $strDateMax = $annee . '-' . $mois . '-' . $jour  . ' 00:00';
            try {
                $dateMax = new DateTime($strDateMax);
                $qb->andWhere('ev.dateHeureDebut < :dateMax')
                    ->setParameter('dateMax', $dateMax);
            } catch (Exception $e) {
                throw new RuntimeException('Los filtros no funcionaron bien');
            }

        }

        return $qb;
    }
}
