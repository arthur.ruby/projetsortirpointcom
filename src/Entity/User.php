<?php

namespace App\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="app_user")
 * @UniqueEntity(fields={"pseudo"}, message="Ce pseudo est déjà utilisé, choisissez-en un autre")
 * @UniqueEntity(fields={"email"}, message="Cet email est déjà utilisé, choisissez-en un autre")
 * @Vich\Uploadable
 */
class User implements UserInterface, Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank( message="Le nom est requis")
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="2", max="255", minMessage="Le nom est trop petit", maxMessage="Le nom ne peut pas dépasser {{ limit }} caractères")
     */
    private $nom;

    /**
     * @Assert\NotBlank( message="Le prénom est requis")
     * @Assert\Length(min="2", max="255", minMessage="Le nom est trop petit", maxMessage="Le nom ne peut pas dépasser {{ limit }} caractères")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\NotBlank( message="Le téléphone est requis")
     * @Assert\Length(min="10", max="13",
     *     minMessage="Le numéro renseigné doit être composé d'au moins {{ limit }} chiffres",
     *     maxMessage="Le numéro renseigné doit être composé au maximum de {{ limit }} chiffres" )
     * @Assert\Regex(pattern="/^(\+|)+([0-9]+( |)){10,14}$/i")
     */
    private $telephone;

    //TODO ajouter regEx sur email
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Length(min="2", max="255", minMessage="Le nom est trop petit", maxMessage="Le nom ne peut pas dépasser {{ limit }} caractères")
     * @Assert\NotBlank( message="Le champ est requis")
     * @Assert\Email(message="Le format de l'email n'est pas correct")
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $admin;

    /**
     * @ORM\Column(type="boolean")
     */
    private $actif;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank( message="Le pseudo est requis")
     * @Assert\Length(min="5", max="60",
     *     minMessage="Le pseudo doit faire au moins {{ limit }} caratères",
     *     maxMessage="Le pseudo doit faire maximum {{ limit }} caratères")
     */
    private $pseudo;

    //TODO ajouter regEx sur mot de passe
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank( message="Le mot de passe est requis")
     * @Assert\Length(min="8", max="80",
     *     minMessage="Le mot de passe doit faire au moins {{ limit }} caratères",
     *     maxMessage="Le mot de passe doit faire maximum {{ limit }} caratères")
     */
    private $motDePasse;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Site", inversedBy="users")
     */
    private $site;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Event", inversedBy="users")
     */
    private $eventInscrit;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event", mappedBy="organisateur", orphanRemoval=true)
     */
    private $eventsOrganised;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * @Assert\File(maxSize="5M", mimeTypes={"image/jpeg", "image/gif", "image/png"}, maxSizeMessage="C'est trop gros", mimeTypesMessage="Le fichier doit être au format jpeg, gif ou png")
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int|null
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTimeInterface|null
     */
    private $updatedAt;



    public function __construct()
    {
        $this->eventInscrit = new ArrayCollection();
        $this->eventsOrganised = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getMotDePasse(): ?string
    {
        return $this->motDePasse;
    }

    public function setMotDePasse(string $motDePasse): self
    {
        $this->motDePasse = $motDePasse;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        if ($this->admin)
        {
	    	return (['ROLE_ADMIN']);
        }
        elseif ($this->actif)
        {
	    	return (['ROLE_ACTIF']);
        }
        else
        {
	    	return (['ROLE_USER']);
        }
    }

    /**
     * Méthode utilisée par le LoginFormAuthenticator pour récupérer le mot de passe de l'entité
     * @inheritDoc
     */
    public function getPassword()
    {
       return $this->getMotDePasse();
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
       return null;
    }

    /** Méthode utilisée par le LoginFormAuthenticator pour récupérer le nom de la personne connectée
     * @inheritDoc
     */
    public function getUsername()
    {
       return $this->getPseudo();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {

    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEventInscrit(): Collection
    {
        return $this->eventInscrit;
    }

    public function addEventInscrit(Event $eventInscrit): self
    {
        if (!$this->eventInscrit->contains($eventInscrit)) {
            $this->eventInscrit[] = $eventInscrit;
        }

        return $this;
    }

    public function removeEventInscrit(Event $eventInscrit): self
    {
        if ($this->eventInscrit->contains($eventInscrit)) {
            $this->eventInscrit->removeElement($eventInscrit);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEventsOrganised(): Collection
    {
        return $this->eventsOrganised;
    }

    public function addEventsOrganised(Event $eventsOrganised): self
    {
        if (!$this->eventsOrganised->contains($eventsOrganised)) {
            $this->eventsOrganised[] = $eventsOrganised;
            $eventsOrganised->setOrganisateur($this);
        }

        return $this;
    }

    public function removeEventsOrganised(Event $eventsOrganised): self
    {
        if ($this->eventsOrganised->contains($eventsOrganised)) {
            $this->eventsOrganised->removeElement($eventsOrganised);
            // set the owning side to null (unless already changed)
            if ($eventsOrganised->getOrganisateur() === $this) {
                $eventsOrganised->setOrganisateur(null);
            }
        }

        return $this;
    }


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|UploadedFile|null $imageFile
     * @throws Exception
     */
    public function setImageFile($imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

	/**
	 * @inheritDoc
	 */
	public function serialize()
	{
		return serialize([$this->id, $this->getUsername(), $this->getPassword(), $this->imageName]);
	}

	/**
	 * @inheritDoc
	 */
	public function unserialize($serialized)
	{
		list($this->id, $this->pseudo, $this->motDePasse, $this->imageName) = unserialize($serialized);
	}
}
